package com.bosch.appcrunch.boschrangers.compose.ui.theme

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight


//val Bosch = FontFamily(
//    Font(R.font.bosch_sans),
//    Font(R.font.bosch_sans_regular, style = FontStyle.Normal),
//    Font(R.font.bosch_sans_black, weight = FontWeight.Black),
//    Font(R.font.bosch_sans_bold, weight = FontWeight.Bold),
//    Font(R.font.bosch_sans_bold_italic, weight = FontWeight.Bold, style = FontStyle.Italic),
//    Font(R.font.bosch_sans_black_italic, weight = FontWeight.Black, style = FontStyle.Italic),
//    Font(R.font.bosch_sans_regular_italic, FontWeight.Normal, style = FontStyle.Italic)
//)
