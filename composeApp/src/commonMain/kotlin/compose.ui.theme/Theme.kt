package compose.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.graphics.Color
import com.bosch.appcrunch.boschrangers.compose.ui.theme.BoschTypography
import com.bosch.appcrunch.boschrangers.compose.ui.theme.LocalSpacing
import com.bosch.appcrunch.boschrangers.compose.ui.theme.LocalTextColor
import com.bosch.appcrunch.boschrangers.compose.ui.theme.Shapes
import com.bosch.appcrunch.boschrangers.compose.ui.theme.Spacing
import com.bosch.appcrunch.boschrangers.compose.ui.theme.TextColor


private val PTColorPalette = lightColors(
    primary = BoschDarkBlueB50,
    primaryVariant = BoschViolet,
    secondary = BoschBlue,
    background = BoschGray95,
    surface = BoschWhite,
    onPrimary = BoschWhite,
    onSecondary = BoschWhite,
    onBackground = BoschBlue10,
    onSurface =BoschDarkBlueB75,
)

private val AAColorPalette = lightColors(
    primary = BoschTurquoise,
    primaryVariant = BoschTurquoise,
    secondary = BoschLightGray,
    background = BoschGray95,
    surface = BoschWhite,
    onPrimary = BoschWhite,
    onSecondary = BoschBlack,
    onBackground = Color.Black,
    onSurface = BoschGreen,
)

@Composable
fun BoschrangersgenericTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    useAlternativeTheme: Boolean = false,
    content: @Composable() () -> Unit
) {

    // val colors = if(useAlternativeTheme) PTColorPalette else PTColorPalette

    CompositionLocalProvider(
        LocalSpacing provides Spacing(),
        LocalTextColor provides TextColor(),
    ) {
        MaterialTheme(
            colors = PTColorPalette,
            typography = BoschTypography,
            shapes = Shapes,
            content = content
        )
    }
}