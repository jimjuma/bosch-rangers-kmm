package com.bosch.appcrunch.boschrangers.compose.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp

data class TextSize(
    val small: TextUnit = 16.sp,
    val normal: TextUnit = 18.sp,
    val medium: TextUnit = 24.sp,
    val large: TextUnit = 26.sp
)


val LocalTextSize = compositionLocalOf { TextSize() }

val MaterialTheme.TextSize: TextSize
    @Composable
    @ReadOnlyComposable
    get() = LocalTextSize.current