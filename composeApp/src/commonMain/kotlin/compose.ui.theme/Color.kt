package compose.ui.theme

import androidx.compose.ui.graphics.Color

val BoschRed = Color(0xFFED0007)
val BoschPurple = Color(0xFF9E2896)
val BoschViolet = Color(0xFF50237F)
val BoschBlue = Color(0xFF007BC0)
val BoschTurquoise = Color(0xFF18837E)
val BoschGreen = Color(0xFF00884A)
val BoschDarkGreen = Color(0xFF006249)

// Neutral secondary
val BoschWhite = Color(0xFFFFFFFF)
val BoschBlack = Color(0xFF000000)
val BoschDarkGray = Color(0xFF525F6B)
val BoschLightGray = Color(0xFFBFC0C2)

// Functional Colors
val BoschYellow = Color(0xFFFCAF17)

// Color variations
//val BoschDarkBlueB50 = Color(0xFF002B49)
val BoschDarkBlueB50 = Color(0xFF004975)
val BoschDarkBlueB75 = Color(0xFF001624)

val BoschBlue10 = Color(0xFF001D33)
val BoschGray95 = Color(0xFFEFF1F2)
val BoschGray35 = Color(0xFF4E5256)

val Background = BoschDarkBlueB50
val CardBackGround = BoschGray95

// Additional colors
val BoschGreen95 = Color(0xFFE2F5E7)