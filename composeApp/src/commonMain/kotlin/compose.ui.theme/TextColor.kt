package com.bosch.appcrunch.boschrangers.compose.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.graphics.Color
import compose.ui.theme.*

data class TextColor(
    val default: Color = BoschBlue10,
    val hint: Color = BoschDarkGray,
    val label: Color = BoschLightGray,
    val red: Color = BoschRed,
)

val LocalTextColor = compositionLocalOf { TextColor() }

val MaterialTheme.TextColor: TextColor
    @Composable
    @ReadOnlyComposable
    get() = LocalTextColor.current