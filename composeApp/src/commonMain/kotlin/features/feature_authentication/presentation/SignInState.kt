package features.feature_authentication.presentation

data class SignInState(
    val isLoading: Boolean = false,
    val isPasswordVisible: Boolean = false,
    val username: String = "",
    val usernameError: String? = null,
    val password: String = "",
    val passwordError: String? = null,
    val error: String? = null,
)