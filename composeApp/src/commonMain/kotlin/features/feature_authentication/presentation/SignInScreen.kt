@file:OptIn(ExperimentalResourceApi::class)

package features.feature_authentication.presentation

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.IconButton
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import compose.ui.theme.Background
import compose.ui.theme.BoschBlack
import compose.ui.theme.BoschRed
import compose.ui.theme.BoschTurquoise
import compose.ui.theme.BoschWhite
import features.home.HomeScreen
import features.home.HomeToolbar
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource

class SignInScreen : Screen {
    @OptIn(ExperimentalResourceApi::class)
    @Composable
    override fun Content() {

        val navigator = LocalNavigator.currentOrThrow

        Scaffold(backgroundColor = MaterialTheme.colors.primary) {

            Column(
                Modifier
                    .fillMaxSize()
                    .background(BoschWhite),
                verticalArrangement = Arrangement.Top
            ) {

                Image(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(10.dp),
                    contentScale = ContentScale.Crop,
                    painter = painterResource("bosch_supergraphic.xml"),
                    contentDescription = "Bosch supergraphic"
                )
                Image(
                    modifier = Modifier
                        .width(150.dp)
                        .height(100.dp),
                    painter = painterResource("bosch_symbol_logo_black_red_en.xml"),
                    contentDescription = "Bosch logo"
                )
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally),
                    contentAlignment = Alignment.Center
                ) {
                    LoginLogo()
                }
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    text = "Welcome",
                    style = MaterialTheme.typography.h4,
                    fontWeight = FontWeight.Bold,
                    color = Background
                )

                Column(
                    Modifier
                        .fillMaxSize()
                        .padding(16.dp)
                ) {

                    Column(
                        Modifier.fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        OutlinedTextField(
                            modifier = Modifier.fillMaxWidth(),
                            value = "",
                            isError = false,
                            onValueChange = {

                            },
                            label = { Text(text = "Username") },
                            singleLine = true,
                            textStyle = TextStyle(
                                color = Color.Black,
                                textDecoration = TextDecoration.None
                            )
                        )

                        Spacer(modifier = Modifier.height(8.dp))
                        OutlinedTextField(
                            modifier = Modifier.fillMaxWidth(),
                            value = "",
                            isError = false,
                            onValueChange = {

                            },
                            label = { Text(text = "Password") },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Password,
                                imeAction = ImeAction.Done
                            ),
                            visualTransformation = if (true) VisualTransformation.None else PasswordVisualTransformation(),
                            trailingIcon = {
//                                val icon = if (state.isPasswordVisible) {
//                                    Icons.Filled.Visibility
//                                } else {
//                                    Icons.Filled.VisibilityOff
//                                }
                                IconButton(onClick = {

                                }) {

                                }
                            },
                            textStyle = TextStyle(color = Color.Black)
                        )

                        Spacer(modifier = Modifier.height(16.dp))

                        Spacer(modifier = Modifier.height(4.dp))
                        if (false) {
                            LinearProgressIndicator(
                                modifier = Modifier
                                    .align(Alignment.CenterHorizontally)
                                    .fillMaxWidth()
                            )
                        }
                        Spacer(modifier = Modifier.weight(1f))
                        Spacer(modifier = Modifier.height(4.dp))
                        TermsAndPrivacyPolicy(
                            onTermsClick = {
                                // navigator.navigate(TermsScreenDestination())
                            },
                            onPrivacyPolicyClick = {
                                // navigator.navigate(PrivacyStatementScreenDestination())
                            })

                        Divider(
                            modifier = Modifier.fillMaxWidth(),
                            color = Color.LightGray
                        )
                        Button(
                            enabled = true,
                            onClick = {
                                navigator.push(HomeScreen())
                            },
                            modifier = Modifier
                                .padding(6.dp)
                                .fillMaxWidth(),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = MaterialTheme.colors.primary,
                                contentColor = MaterialTheme.colors.onPrimary
                            )
                        ) {
                            Text(
                                text = "Login", color = BoschWhite,
                                modifier = Modifier
                                    .padding(6.dp),
                                style = MaterialTheme.typography.button
                            )
                        }

                    }
                }

            }
        }
    }

    @Composable
    private fun TermsAndPrivacyPolicy(
        onTermsClick: () -> Unit,
        onPrivacyPolicyClick: () -> Unit
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "By logging into your account, you agree to",
                style = MaterialTheme.typography.caption,
                color = BoschBlack
            )
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(4.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Bosch’s",
                style = MaterialTheme.typography.caption,
                color = BoschBlack
            )
            Text(
                text = " Terms of Service ",
                style = MaterialTheme.typography.caption,
                color = BoschTurquoise,
                modifier = Modifier
                    .clip(
                        RoundedCornerShape(4.dp)
                    )
                    .clickable { onTermsClick() }
            )
            Text(
                text = "&",
                style = MaterialTheme.typography.caption,
                color = BoschBlack
            )
            Text(
                text = " Data Protection Policy ",
                style = MaterialTheme.typography.caption,
                color = BoschTurquoise,
                modifier = Modifier
                    .clip(
                        RoundedCornerShape(4.dp)
                    )
                    .clickable { onPrivacyPolicyClick() }
            )
        }
    }

    @OptIn(ExperimentalResourceApi::class)
    @Composable
    private fun LoginLogo() {
        Image(
            painterResource("ranger_logo.jpg"),
            contentDescription = "Brand Logo",
            modifier = Modifier
                .padding(top = 26.dp, bottom = 32.dp)
                .size(120.dp)
                .clip(RoundedCornerShape(4.dp))
        )
    }
}
