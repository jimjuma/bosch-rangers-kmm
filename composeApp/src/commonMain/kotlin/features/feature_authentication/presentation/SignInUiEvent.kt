package features.feature_authentication.presentation

sealed class SignInUiEvent{
    data class UsernameChange(val username: String): SignInUiEvent()
    data class PasswordChange(val password: String): SignInUiEvent()
    object Submit: SignInUiEvent()
    object ChangePasswordVisibility: SignInUiEvent()
}
