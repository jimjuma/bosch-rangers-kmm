package features.feature_authentication.presentation

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import cafe.adriel.voyager.core.model.ScreenModel

class SignInViewModel: ScreenModel {
    private val _state = mutableStateOf(SignInState())
    public val state: State<SignInState> = _state
    fun onEventChange(event: SignInUiEvent){
        when(event){
            is SignInUiEvent.UsernameChange -> {
                _state.value = state.value.copy(
                    username = event.username
                )
            }
            is SignInUiEvent.PasswordChange -> {
                _state.value = state.value.copy(
                    password = event.password
                )
            }
            is SignInUiEvent.Submit -> {
                _state.value = state.value.copy(
                    isLoading = true
                )

            }
            is SignInUiEvent.ChangePasswordVisibility -> {
                _state.value = state.value.copy(
                    isPasswordVisible = !state.value.isPasswordVisible
                )
            }
        }
    }
}