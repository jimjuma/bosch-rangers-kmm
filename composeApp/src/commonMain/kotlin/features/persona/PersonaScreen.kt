package features.persona

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import common.util.LocalAppNavigator
import compose.ui.theme.Background
import compose.ui.theme.BoschWhite
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource

class PersonaScreen : Screen {

    @OptIn(ExperimentalResourceApi::class)
    @Composable
    override fun Content() {
        val navigator = LocalAppNavigator.current
        Scaffold {
            Column {

                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(BoschWhite)
                ) {
                    Column {
                        Row(
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                            modifier = Modifier
                                .padding(
                                    15.dp, 8.dp, 15.dp, 8.dp
                                )
                                .fillMaxWidth()
                        ) {
                            Icon(
                                imageVector = Icons.Default.ArrowBack,
                                contentDescription = "Back",
                                modifier = Modifier
                                    .size(20.dp)
                                    .clickable {
                                        navigator?.pop()
                                    },
                                tint = Background
                            )

                            Text(
                                text = "Persona",
                                style = MaterialTheme.typography.h6,
                                modifier = Modifier.padding(4.dp),
                                color = Background,
                                fontWeight = FontWeight.Bold
                            )

                            Icon(
                                imageVector = Icons.Default.Search,
                                contentDescription = "Search",
                                modifier = Modifier
                                    .size(20.dp)
                                    .clickable {
                                    },
                                tint = Background
                            )
                        }
                        Image(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(5.dp),
                            contentScale = ContentScale.Crop,
                            painter = painterResource("bosch_supergraphic.xml"),
                            contentDescription = "Bosch supergraphic"
                        )
                    }
                }

                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(BoschWhite),
                    contentAlignment = Alignment.TopEnd
                ) {
                    Icon(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(20.dp),
                        imageVector = Icons.Filled.List,
                        tint = Color.Black,
                        contentDescription = "Filter list"
                    )
                }

                LazyColumn(
                    modifier = Modifier.fillMaxWidth(),
                    contentPadding = PaddingValues(16.dp)
                ) {
                    items(count = 10) {
                        Column(
                            modifier = Modifier.fillMaxWidth()
                                .wrapContentHeight()
                                .padding(vertical = 5.dp)
                        ) {
                            Text(
                                "Max Mayer",
                                modifier = Modifier.padding(3.dp),
                                fontWeight = FontWeight.Bold,
                                color = Color.Black
                            )
                            Text(
                                "Sub dealer",
                                color = Color.Black, modifier = Modifier.padding(3.dp)
                            )
                            Text(
                                "+254757573492",
                                color = Color.DarkGray, modifier = Modifier.padding(3.dp)
                            )
                            Spacer(modifier = Modifier.height(10.dp))
                            Divider()
                        }
                    }
                }
            }
        }
    }
}