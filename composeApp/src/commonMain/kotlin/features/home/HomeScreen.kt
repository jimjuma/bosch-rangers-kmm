package features.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import compose.ui.theme.Background
import compose.ui.theme.BoschWhite
import features.persona.PersonaScreen
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.painterResource

class HomeScreen : Screen {
    @OptIn(ExperimentalResourceApi::class)
    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow

        Scaffold {
            Column {
                HomeToolbar() {}
                Spacer(modifier = Modifier.height(10.dp))

                val homeItemsList = listOf(
                    HomeItemTile(
                        Icons.Default.Person,
                        "Persona",
                        PersonaScreen()
                    ),
                    HomeItemTile(
                        Icons.Filled.Info,
                        "Questionnaires"
                    ),
                    HomeItemTile(
                        Icons.Default.Person,
                        "Products"
                    ),
                    HomeItemTile(
                        Icons.Default.Person,
                        "Sales"
                    ),
                    HomeItemTile(
                        Icons.Default.Person,
                        "TMTK"
                    ),
                    HomeItemTile(
                        Icons.Default.Person,
                        "Surveys"
                    ),
                )


                LazyVerticalGrid(
                    columns = GridCells.Fixed(2),
                    modifier = Modifier.padding(10.dp),
                    verticalArrangement = Arrangement.spacedBy(16.dp),
                    horizontalArrangement = Arrangement.spacedBy(16.dp)
                ) {
                    items(
                        homeItemsList
                    ) { item ->

                        Card {
                            Column(
                                modifier = Modifier.padding(5.dp).clickable {
                                    if (item.screen != null) {
                                        navigator.push(item.screen)
                                    }
                                },
                                horizontalAlignment = Alignment.CenterHorizontally
                            ) {

                                Icon(
                                    item.image,
                                    item.toString(),
                                    modifier = Modifier
                                        .width(150.dp)
                                        .height(100.dp),
                                )
                                Spacer(modifier = Modifier.height(5.dp))
                                Text(
                                    item.name,
                                    modifier = Modifier.padding(5.dp),
                                    textAlign = TextAlign.Center
                                )
                            }
                        }
                    }

                }
            }
        }

    }
}


@OptIn(ExperimentalResourceApi::class)
@Composable
fun HomeToolbar(
    name: String = "",
    goToProfile: () -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(BoschWhite)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .padding(
                    15.dp, 8.dp, 15.dp, 8.dp
                )
                .fillMaxWidth()
        ) {


            Column(
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Bosch Rangers",
                    style = MaterialTheme.typography.h6,
                    modifier = Modifier.padding(4.dp),
                    color = Background,
                    fontWeight = FontWeight.Bold
                )
            }
            Icon(
                imageVector = Icons.Default.AccountCircle,
                contentDescription = "Profile",
                modifier = Modifier
                    .size(40.dp)
                    .clickable {
                        goToProfile()
                    },
                tint = Background
            )
        }
    }

    Image(
        modifier = Modifier
            .fillMaxWidth()
            .height(7.dp),
        contentScale = ContentScale.Crop,
        painter = painterResource("bosch_supergraphic.xml"),
        contentDescription = "Bosch supergraphic"
    )
}

data class HomeItemTile(
    val image: ImageVector,
    val name: String,
    val screen: Screen? = null
)